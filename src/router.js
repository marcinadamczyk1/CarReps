import Vue from 'vue';
import VueRouter from 'vue-router';
import Start from './components/StartComponent';
import Clients from './components/ClientsComponent';
import Repairs from './components/RepairsComponent';
import Cars from './components/CarsComponent';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    base: __dirname,
    routes:[
        {path: '/', name:'start', component: Start},
        {path: '/clients', name:'clients', component: Clients},
        {path: '/repairs', name: "repairs", component: Repairs},
        {path: '/cars', name: 'cars', component: Cars}
    ]
});

export default router;